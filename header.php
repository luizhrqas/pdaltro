<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>Pedro Daltro</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/perfect-scrollbar.min.css" rel="stylesheet">
    


    <!-- Custom styles for this template -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="assets/js/hover.zoom.js"></script>
    <script src="assets/js/hover.zoom.conf.js"></script>
    <script src="assets/js/perfect-scrollbar.min.js"></script>
    <script src="assets/js/mail.js"></script>

    <!-- FEED DELICIOUS -->

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.md5.js"></script>
    <script type="text/javascript" src="assests/js/delicious.js"></script>

    <!-- FEED PINTEREST -->

    <script type="text/javascript" src="assests/js/<php_pinterest_feed class="php"></php_pinterest_feed>"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

<!-- PERFECT SCROLL -->

  <style>
  .box-com-scroll  { 
    position: relative;
    height: 100%; /* Or whatever you want (eg. 400px) */
    overflow: hidden;
  }
  </style> <!-- FIM PERFECT SCROLL -->

  <body>

    <!-- Static navbar -->
    <div class="navbar navbar-inverse navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="menu-principal navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-justified">
            <li><a href="index.php" class='<?php echo ($atual == "hd_externo") ? "active" : ""; ?>'>HD Externo</a></li>
            <li><a href="trabalhos.php" class='<?php echo ($atual == "trabalhos") ? "active" : ""; ?>'>Trabalhos</a></li>
            <li><a href="contato.php" class='<?php echo ($atual == "contato") ? "active" : ""; ?>'>Contato</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
      <div class="row mt centered no-mb"> 
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <img class="img-responsive" src="assets/img/logo.png" style="max-height: 55px;" alt="Pedro Daltro" />
          </div>

          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 busca">

            <form action="">

              <div class="right-inner-addon">
                <button type="submit" class="btn-pesquisar">
                  <i class="fa fa-search"></i>
                </button>
                <input type="search" class="form-control input-lg campo-pesquisa" placeholder="Pesquisar" />
              </div>

            </form>
            
          </div>
              
      </div><!-- /row -->
    </div><!-- /container -->